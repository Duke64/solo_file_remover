# solo_file_remover

## ToDo
+ TDD
    + `Setup` and `Teardown` are not async
+ error handling
+ user arguments
    + variable file endings instead of hardcoded `.jpg` and `.awr`
    + dir location
+ user cli
+ flags / parameters
    + recusrive with `-r`
    + show output with `-s`

## Scratchpad
+ using https://crates.io/crates/fs-err