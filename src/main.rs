use std::collections::HashSet;
use std::fs;
use std::path::{Path, PathBuf};
use clap::Parser;

#[derive(Debug, Parser)]
struct Args {
    #[clap(parse(from_os_str))]
    path: PathBuf
}

fn main() {
    let args: Args = Args::parse();
    let path: &Path = args.path.as_path();

    let files_within_dir: HashSet<PathBuf> = visit_dirs(&path);
    let files_to_delete: Vec<PathBuf> = collect_mismatching_raws(&files_within_dir);
    
    delete_files_on_filesystem(&files_to_delete);
}

fn visit_dirs(dir: &Path) -> HashSet<PathBuf> {

    let mut files_within_dir: HashSet<PathBuf> = HashSet::new();

    if dir.is_dir() {
        for entry in fs::read_dir(dir).unwrap() {
            if entry.as_ref().unwrap().metadata().unwrap().is_file() {
                let path = entry.unwrap().path();

                files_within_dir.insert(path);
            }
        }
    }

    files_within_dir
}

fn collect_mismatching_raws(file_list: &HashSet<PathBuf>) -> Vec<PathBuf> {   

    let mut files_to_delete: Vec<PathBuf> = Vec::new();
    
    for raw in file_list.iter().filter(|x| x.extension().unwrap_or_default() == "awr" ) {
        
        let file_stem = raw.file_stem().unwrap();
        let mut file_associate = PathBuf::from(file_stem);
        file_associate.set_extension("jpg");

        let associate_string = file_associate.to_str().unwrap();

        if !file_list.iter().any(
            |x| x.file_name().unwrap() == associate_string){

                files_to_delete.push(raw.clone());
            }
    }

    files_to_delete
}

fn delete_files_on_filesystem(files_to_delete: &Vec<PathBuf>) -> bool {

    for file in files_to_delete {
        fs::remove_file(file).unwrap();
    }

    true
}

#[cfg(test)]
mod tests {
    use std::io;
    use std::fs::{File};

    use super::*;

    fn setup_test_environment() -> io::Result<()> {
        fs::create_dir("ressources")?;

        File::create("ressources/01.jpg")?;

        File::create("ressources/02.jpg")?;
        File::create("ressources/02.awr")?;

        File::create("ressources/03.awr")?;

        Ok(())
    }

    fn tear_down_test_environment() -> io::Result<()> {
        fs::remove_dir_all("ressources")?;

        Ok(())
    }

    fn setup_subdir() -> io::Result<()> {

        fs::create_dir("ressources/subdir")?;

        File::create("ressources/subdir/aa.jpg")?;

        File::create("ressources/subdir/bb.jpg")?;
        File::create("ressources/subdir/bb.awr")?;

        File::create("ressources/subdir/cc.awr")?;

        Ok(())
    }

    #[test]
    fn files_in_directory_should_collected_correctly() {

        // arrange
        let path = Path::new("ressources");
        let files_within_dir;

        setup_test_environment();

        // act
        files_within_dir = visit_dirs(&path);

        // assert
        assert_eq!(files_within_dir.len(), 4);
        
        let reference_file00: PathBuf = PathBuf::from("ressources/01.jpg");        
        let reference_file01: PathBuf = PathBuf::from("ressources/02.jpg");
        let reference_file02: PathBuf = PathBuf::from("ressources/02.awr");
        let reference_file03: PathBuf = PathBuf::from("ressources/03.awr");

        let file00_in_dir: &PathBuf = files_within_dir.get(&reference_file00).unwrap();
        let file01_in_dir: &PathBuf = files_within_dir.get(&reference_file01).unwrap();
        let file02_in_dir: &PathBuf = files_within_dir.get(&reference_file02).unwrap();
        let file03_in_dir: &PathBuf = files_within_dir.get(&reference_file03).unwrap();

        assert_eq!(file00_in_dir.file_name().unwrap(), "01.jpg");
        assert_eq!(file01_in_dir.file_name().unwrap(), "02.jpg");
        assert_eq!(file02_in_dir.file_name().unwrap(), "02.awr");
        assert_eq!(file03_in_dir.file_name().unwrap(), "03.awr");
        
        tear_down_test_environment();
    }

    #[test]
    fn files_in_subdirs_should_not_collected() {

        // arrange
        let path = Path::new("ressources");
        let files_within_dir;

        setup_test_environment();
        setup_subdir();

        // act
        files_within_dir = visit_dirs(&path);

        // assert
        assert_eq!(files_within_dir.len(), 4);
        
        let reference_file00: PathBuf = PathBuf::from("ressources/01.jpg");        
        let reference_file01: PathBuf = PathBuf::from("ressources/02.jpg");
        let reference_file02: PathBuf = PathBuf::from("ressources/02.awr");
        let reference_file03: PathBuf = PathBuf::from("ressources/03.awr");

        let file00_in_dir: &PathBuf = files_within_dir.get(&reference_file00).unwrap();
        let file01_in_dir: &PathBuf = files_within_dir.get(&reference_file01).unwrap();
        let file02_in_dir: &PathBuf = files_within_dir.get(&reference_file02).unwrap();
        let file03_in_dir: &PathBuf = files_within_dir.get(&reference_file03).unwrap();

        assert_eq!(file00_in_dir.file_name().unwrap(), "01.jpg");
        assert_eq!(file01_in_dir.file_name().unwrap(), "02.jpg");
        assert_eq!(file02_in_dir.file_name().unwrap(), "02.awr");
        assert_eq!(file03_in_dir.file_name().unwrap(), "03.awr");
        
        tear_down_test_environment();
    }

    #[test]
    fn mismatching_raws_should_collected(){

        let files_to_delete: Vec<PathBuf>;

        //arrange
        let dir = Path::new("ressources");
        setup_test_environment();

        let mut files_within_dir: HashSet<PathBuf> = HashSet::new();

        if dir.is_dir() {
            for entry in fs::read_dir(dir).unwrap() {
            let path = entry.unwrap().path();

            files_within_dir.insert(path);
            }
        }

        // act
        files_to_delete = collect_mismatching_raws(&files_within_dir);

        // assert
        assert_eq!(files_to_delete.len(), 1);
        
        let file00: &PathBuf = &files_to_delete[0];

        assert_eq!(file00.file_name().unwrap(), "03.awr");

        tear_down_test_environment();
    }

    #[test]
    fn files_should_deleted_from_filesystem() {

        // arrange
        setup_test_environment();

        let mut files_to_delete: Vec<PathBuf> = Vec::new();
        let reference_file00: PathBuf = PathBuf::from("ressources/01.jpg");
        let reference_file01: PathBuf = PathBuf::from("ressources/03.awr");
        files_to_delete.push(reference_file00);
        files_to_delete.push(reference_file01);

        let is_delete_successful: bool;
        let dir = Path::new("ressources");
        let mut not_deleted_files_within_dir: Vec<PathBuf> = Vec::new();

        // act
        is_delete_successful = delete_files_on_filesystem(&files_to_delete);

        // assert
        assert!(is_delete_successful);

            // collect the survived/not deleted files
        if dir.is_dir() {
            for entry in fs::read_dir(dir).unwrap() {
            let path = entry.unwrap().path();

            not_deleted_files_within_dir.push(path);
            }
        }

        assert_eq!(not_deleted_files_within_dir.len(), 2);

        let file00_in_dir: &PathBuf = &not_deleted_files_within_dir[0];
        let file01_in_dir: &PathBuf = &not_deleted_files_within_dir[1];

        assert_eq!(file00_in_dir.file_name().unwrap(), "02.jpg");
        assert_eq!(file01_in_dir.file_name().unwrap(), "02.awr");

        tear_down_test_environment();
    }
}