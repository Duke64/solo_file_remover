#!/usr/bin/env python

import os
import shutil

folderPath = 'ressources'

if os.path.exists(folderPath):
    shutil.rmtree(folderPath)

os.system('cargo test -- --test-threads 1')
